<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Route::get('/profile', function () {
	return view('/profile/index');
});

Route::get('/profile/cabinet', function () {
	return view('/profile/index');
});

Route::get('/profile/change-rate', function () {
	return view('/profile/change-rate');
});

Route::get('/profile/messages', function () {
	return view('/profile/messages');
});

Route::get('/profile/help', function () {
	return view('/profile/help');
});

Route::get('/profile/payment-history', function () {
	return view('/profile/payment-history');
});

Route::get('/profile/service-connection', function () {
	return view('/profile/service-connection');
});

Route::get('/posts/akcii-i-bonusnye-programmy', function () {
	return view('/pages/posts');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');