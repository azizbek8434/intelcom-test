<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="/cabinet/js/cabinet.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js" type="text/javascript"></script>
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Icons -->
    <link rel="shortcut icon" href="/cabinet/favicon.gif" type="image/x-icon">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext"
        rel="stylesheet">

    <!-- Styles -->
		<link rel="stylesheet" href="/cabinet/slick/slick.css">
		<link rel="stylesheet" href="/cabinet/slick/slick-theme.css">
    <link rel="stylesheet" href="/cabinet/css/cabinet.css">
    <link rel="stylesheet" href="/cabinet/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

</head>
