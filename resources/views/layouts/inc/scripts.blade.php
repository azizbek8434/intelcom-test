<script>
    $(window).on("load resize", function () {
        var maxHeight = 0;
        $("#balans-section").find(".balans-section-field").height("auto").each(function () {
            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        }).height(maxHeight);

        var maxHeight = 0;
        $("#cabinet-page-akcii").find(".cabinet-page-akcii-content").height("auto").each(function () {
            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        }).height(maxHeight);

    });

</script>
<script>
    $(function () {
        $('#block-calendar-input').datetimepicker({
            timepicker: false,
            formatDate: 'd.m.Y',
            inline: true
        });
        $('#unlock-calendar-input').datetimepicker({
            timepicker: false,
            formatDate: 'd.m.Y',
            inline: true
        });
        $('#connect-calendar-input').datetimepicker({
            timepicker: false,
            formatDate: 'd.m.Y',
            inline: true
        });
        $.datetimepicker.setLocale('ru');
    });

</script>
<script>
    $('.close-form, .back-form').on('click', function () {
        $('#block-calendar').fadeOut('fast'),
            $('#unlock-calendar').fadeOut('fast'),
            $('#connect-calendar').fadeOut('fast')
    });
    $(".block-account").click(function () {
        $("#block-calendar").fadeIn(300);
    });
    $(".unlock-account").click(function () {
        $("#unlock-calendar").fadeIn(300);
    });
    $(".connect-account").click(function () {
        $("#connect-calendar").fadeIn(300);
    });

</script>
<script src="/cabinet/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $(document).on('ready', function () {
        $(".page-knowlange-base-slider").slick({

            dots: true,
            arrows: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            autoplay: true,
            autoplaySpeed: 5000
        });
    });

</script>

<script type="text/javascript">
    ! function (i) {
        var o, n;
        i(".faq-section-open").on("click", function () {
            o = i(this).parents(".faq-section-block"), n = o.find(".faq-answer"),
                o.hasClass("active_block") ? (o.removeClass("active_block"),
                    n.slideUp()) : (o.addClass("active_block"), n.stop(!0, !0).slideDown(),
                    o.siblings(".active_block").removeClass("active_block").children(
                        ".faq-answer").stop(!0, !0).slideUp())
        })
    }(jQuery);

</script>
<script src="/cabinet/js/menu.js"></script>