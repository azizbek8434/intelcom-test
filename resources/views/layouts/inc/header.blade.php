<header>
    <div class="center">
        <div class="cabinet-header">
            <div class="cabinet-header-logo"><a href="/"><img src="/cabinet/image/logo.png"></a></div>
            <nav class="cabinet-header-nav">
                <div class="cabinet-main-nav">
                    <ul>
                        <li><a href="/profile/messages"><img src="/cabinet/icon/icons8-64.png">Мои сообщения</a></li>
                        <li><a href="/profile/help"><img src="/cabinet/icon/help.png">Помощь</a></li>
                    </ul>
                </div>
            </nav>
            <div id="mobile-menu">
                <div class="hamburger">
                    <button class="button hamburger__button js-menu__toggle">
                        <span class="hamburger__label">Open menu</span>
                    </button>
                </div>
                <nav class="menu">
                    <div class="mobile-menu-first">
                        <ul class="list menu__list">
                            <li><a href="/profile">Мой профиль</a></li>
                            <li><a href="/profile/cabinet">Личный кабинет</a></li>
                            <li><a href="/profile/messages">Мои сообщения</a></li>
                            <li><a href="/profile/help">Помощь</a></li>
                        </ul>
                    </div>
                    <div class="mobile-menu-second">
                        <div class="cabinet-person-ball-plus">
                            <div class="cabinet-name">Мои баллы:</div>
                            <div class="cabiinet-ball-num">327 баллов</div>
                        </div>
                        <div class="cabinet-person-cash-plus">
                            <div class="cabinet-name">Оплата:</div>
                            <a href=""><img src="/cabinet/icon/pay-iocn.png" alt="Оплатить"></a>
                        </div>
                        <div class="cabinet-person-se">
                            <div class="cabinet-person-icon"><img src="/cabinet/icon/person.png"></div>
                            <div class="cabinet-data">
                                <div class="cabinet-name">Иванов И.И.</div>
                                <div class="cabinet-account-number"><span>Лицевой счет</span>№ 312456</div>

                            </div>
                        </div>
                        <div class="mobile-menu-exit">
                            <div class="cabinet-exit"><a href="">Выйти из личного кабинета</a></div>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="cabinet-person">
                <div class="cabinet-person-ball-plus">
                    <div class="cabinet-name">Мои баллы:</div>
                    <div class="cabiinet-ball-num">327 баллов</div>
                </div>
                <div class="cabinet-person-cash-plus">
                    <div class="cabinet-name">Оплата:</div>
                    <a href=""><img src="/cabinet/icon/pay-iocn.png" alt="Оплатить"></a>
                </div>
                <a href="" title="Профиль">
                    <div class="cabinet-person-se">
                        <div class="cabinet-person-icon"><img src="/cabinet/icon/person.png"></div>
                        <div class="cabinet-data">
                            <div class="cabinet-name">Иванов И.И.</div>
                            <div class="cabinet-account-number"><span>Лицевой счет</span>№ 312456</div>

                        </div>
                    </div>
                </a>
                <div class="cabinet-exit"><a href="/login">Выйти</a></div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</header>
