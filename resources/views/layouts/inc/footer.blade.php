<footer class="person-cabinet">
    <div class="center">
        <div class="footer-top">
            <div class="footer-top-tel">
                <a href="tel:88003335033">8-800-333-50-33</a>
                <a href="tel:89282269141">8-928-226-91-41</a>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="mini-footer-block col-md-4">
                <p><a href="" title="">Положение о конфиденциальности и защите персональных данных</a></p>
                <p>© 2008 – 2019 ООО «ИнТелКом»</p>
            </div>
            <div class="mini-footer-block col-md-4">
                <a href="https://makeart.ws" title="Разработка сайта" rel="nofollow" target="_blank">Сайт разработан
                    web-студией MakeArt</a>
            </div>
            <div class="mini-footer-block col-md-4">
                <div class="footer-social">
                    <a href="https://www.instagram.com/intelcom_internet/" target="_blank">
                        <div class="footer-social-block instagram-footer"><img src="/cabinet/icon/instagram-logo.png">
                        </div>
                    </a>
                    <a href="https://tlgg.ru/ITKRBOT" target="_blank">
                        <div class="footer-social-block telegram-footer"><img src="/cabinet/icon/telegram-logo.png">
                        </div>
                    </a>
                    <!--<a href=""><div class="footer-social-block"><img src="/cabinet/icon/vk-social-network-logo.png"></div></a>-->
                    <a href="https://wa.me/79282269141" target="_blank">
                        <div class="footer-social-block whatsapp-footer"><img src="/cabinet/icon/whatsapp-logo.png">
                        </div>
                    </a>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</footer>
<div id="block-calendar">
    <div class="back-form"></div>
    <form class="modal-calendar" method="post" action="">
        <div class="close-form"><img src="/cabinet/icon/close-forn.png"></div>
        <div class="modal-calendar-block">
            <input type="text" name="block-calendar-input" id="block-calendar-input" autocomplete="off" />
        </div>

        <div class="modal-calendar-button">
            <input id="block-button" type="submit" value="Заблокировать" />
        </div>
    </form>
</div>
<div id="unlock-calendar">
    <div class="back-form"></div>
    <form class="modal-calendar" method="post" action="">
        <div class="close-form"><img src="/cabinet/icon/close-forn.png"></div>
        <div class="modal-calendar-block">
            <input type="text" name="unlock-calendar-input" id="unlock-calendar-input" autocomplete="off" />
        </div>

        <div class="modal-calendar-button">
            <input id="block-button" type="submit" value="Заблокировать" />
        </div>
    </form>
</div>
<div id="connect-calendar">
    <div class="back-form"></div>
    <form class="modal-calendar" method="post" action="">
        <div class="close-form"><img src="/cabinet/icon/close-forn.png"></div>
        <div class="modal-calendar-block">
            <input type="text" name="connect-calendar-input" id="connect-calendar-input" autocomplete="off" />
        </div>

        <div class="modal-calendar-button">
            <input id="block-button" type="submit" value="Заблокировать" />
        </div>
    </form>
</div>
