<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.inc.head')

<body id="cabinet-page">
    @include('layouts.inc.header')
    @yield('content')
    @include('layouts.inc.footer')
	@include('layouts.inc.scripts')
</body>

</html>
