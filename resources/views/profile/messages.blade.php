@extends('layouts.app')
@section('content')
<main id="inline-page" class="center">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Главная</a></li>
        <li class="breadcrumb-item active">Мои сообщения</li>
    </ol>
    <section id="my-message">
        <h1>Мои сообщения</h1>
        <div class="my-message-block">
            <p>Подключение и отключение услуг доступно в разделе «Услуги».</p>
        </div>
        <div class="my-message-block">
            <p>Вы сами решаете, что и когда смотреть. С нашим каталогом легко отыскать всё самое интересное!</p>
            <p>Более 200 телеканалов различной тематики! На любой вкус!</p>
        </div>
    </section>
</main>
@endsection
