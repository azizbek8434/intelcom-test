@extends('layouts.app')

@section('content')
    <section id="balans-section">
        <div class="center">
                <div class="balans-section-block col-md-6">
                    <div class="balans-section-field">
                        <h2>Мой баланс</h2>
                        <div class="balans-section-block-date">данные на <span class="balans-section-date">09.04.2019 г.</span><span class="balans-section-time">18:15</span></div>
                        <div class="balans-section-block-money">2000 руб. <div class="balans-section-block-money-arrows"><a href=""><img src="/cabinet/icon/rub-arrows.png" alt="Обновить" title="Обновить"></a></div></div>
                        <div class="balans-section-block-history"><a href=""><img src="/cabinet/icon/list.png">История платежей</a></div>
                    </div>
                </div>
                <div class="balans-section-block col-md-6">
                    <div class="balans-section-field">
                        <h2>Пополнить баланс</h2>
                        <div class="balans-section-pay"><input type="num" value="" placeholder="500"><button type="submit">Оплатить</button></div>
                        <div class="balans-section-pay-info">От 100 до 15 000 руб.</div>
                        <div class="balans-section-pay-balans"><a href=""><img src="/cabinet/icon/wallet.png">Подключить автоплатеж</a></div>
                    </div>
                </div>
                
                <div class="clear"></div>
            
        </div>
    </section>
    <section id="cabinet-page-my-tarif" class="center">
            <h2>Мои услуги и тарифы</h2><div class="cabinet-page-my-tarif-tuning"><a href=""><img src="/cabinet/icon/cogwheel-outline.png"></a></div>
            <div class="clear"></div>
            <div class="col-md-6">
                <div class="cabinet-page-my-tarif-block">
                    <h3>Интернет Безлимитный</h3>
                    <div class="cabinet-page-my-tarif-list">
                        <div class="cabinet-page-my-tarif-no-active">
                            <div class="cabinet-page-my-tarif-no-active-title">Услуга не подключена</div> 
                            <div class="cabinet-page-my-tarif-active-info"><img src="/cabinet/icon/information-tarif.png"><div class="cabinet-page-my-tarif-active-info-hidden">Вы можете подключить тарифы: социальный, комфорт, престиж, vip</div></div>
                            
                        </div>
                        <div class="cabinet-page-my-tarif-active-tarif">Подключить</div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="cabinet-page-my-tarif-block">
                    <h3>Телевидение Интерактивное</h3>
                    <div class="cabinet-page-my-tarif-list">
                        <div class="cabinet-page-my-tarif-no-active">
                            <div class="cabinet-page-my-tarif-no-active-title">Услуга не подключена</div> 
                            <div class="cabinet-page-my-tarif-active-info"><img src="/cabinet/icon/information-tarif.png"><div class="cabinet-page-my-tarif-active-info-hidden">Вы можете подключить тарифы: социальный, оптимальный, увлекательный, продвинутый, #люблюсериалы, премиальный</div></div>
                            
                        </div>
                        <div class="cabinet-page-my-tarif-active-tarif">Подключить</div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="cabinet-page-my-tarif-block">
                    <h3>Интернет Безлимитный</h3>
                    <div class="cabinet-page-my-tarif-list">
                        <div class="cabinet-page-my-tarif-title">Комфорт</div>
                        <div class="cabinet-page-my-tarif-speed">Скорость: до <span>50</span> Мбит/с</div>
                        <div class="cabinet-page-my-tarif-money">Абонентская плата: <span>500</span> руб./мес</div>
                        <div class="cabinet-page-my-tarif-href"><a href="">Сменить тариф</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="cabinet-page-my-tarif-block ">
                    <h3>Телевидение Интерактивное</h3>
                    <div class="cabinet-page-my-tarif-list">
                        <div class="cabinet-page-my-tarif-title">Премиальный</div>
                        <div class="cabinet-page-my-tarif-speed">Телеканалов: <span>27</span> каналов</div>
                        <div class="cabinet-page-my-tarif-money">Абонентская плата: <span>400</span> руб./мес</div>
                        <div class="cabinet-page-my-tarif-href"><a href="">Сменить тариф</a></div>
                        <div class="cabinet-page-my-tarif-href-dop-paket"><a href="">Подключить доп. пакеты</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="cabinet-page-my-tarif-block ">
                    <h3>Статический (белый) адрес IpV4</h3>
                    <div class="cabinet-page-my-tarif-list">
                        
                        <div class="cabinet-page-my-tarif-no-active">
                            <div class="cabinet-page-my-tarif-no-active-title">Услуга не подключена</div> 
                            <div class="cabinet-page-my-tarif-active-info"><img src="/cabinet/icon/information-tarif.png"><div class="cabinet-page-my-tarif-active-info-hidden">Игроманам и продвинутым пользователям</div></div>
                            
                        </div>
                        <div class="cabinet-page-my-tarif-active-tarif">Подключить</div>
                        <div class="clear"></div>
                    </div>
                    <div class="cabinet-page-my-tarif-list">
                        <div class="cabinet-page-my-tarif-ip-text">Ваш IP-адрес: <span>xxx.xx.xxx.xxx</span></div>
                        <div class="cabinet-page-my-tarif-ip-text">Абонентская плата: <span>300</span> руб./мес.</div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
    </section>
    <section id="cabinet-page-akcii" class="center">
        <div class="col-md-6">
            <h2>Акции</h2>
            <a href=""><div class="cabinet-page-akcii-block">
                <div class="cabinet-page-akcii-back"></div>
                 <div class="cabinet-page-akcii-img"><img src="/cabinet/image/3.jpg"></div>
                <div class="cabinet-page-akcii-content">
                    <div class="cabinet-page-akcii-button">Подробнее</div>
                </div>
            </div>
            </a>
            <a href="">
            <div class="cabinet-page-akcii-block"> 
                <div class="cabinet-page-akcii-back"></div>
                <div class="cabinet-page-akcii-img"><img src="/cabinet/image/3.jpg"></div>
                <div class="cabinet-page-akcii-content">
                    <div class="cabinet-page-akcii-button">Подробнее</div>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6">
            <h2>Предложения от партнеров</h2>
            <a href="">
            <div class="cabinet-page-akcii-block">
                <div class="cabinet-page-akcii-back"></div>
                <div class="cabinet-page-akcii-img"><img src="/cabinet/image/1.png"></div>
                <div class="cabinet-page-akcii-content">
                    <div class="cabinet-page-akcii-button">Подробнее</div>
                </div>
            </div></a>
            <a href="">
            <div class="cabinet-page-akcii-block">
                <div class="cabinet-page-akcii-back"></div>
                <div class="cabinet-page-akcii-img"><img src="/cabinet/image/2.jpg"></div>
                <div class="cabinet-page-akcii-content">
                    <div class="cabinet-page-akcii-button">Подробнее</div>
                </div>
            </div>
            </a>
        </div>
        <div class="clear"></div>
    </section>
@endsection