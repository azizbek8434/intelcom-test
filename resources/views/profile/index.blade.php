@extends('layouts.app')

@section('content')
<main id="page-profil">
    <section id="page-profil-info" class="center">
        <h1>Мой профиль</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active">Профиль</li>
        </ol>
        <div class="page-profil-info-block">
            <div class="page-profil-info-image"><img src="cabinet/icon/person.png"></div>
            <div class="page-profil-info-text">
                <div class="page-profil-info-name">Иванов Иван Иванович</div>
                <div class="page-profil-info-number-contact"><span>Договор №:</span> 354567</div>
                <div class="page-profil-info-personal-account"><span>Лицевой счет №:</span> 863863</div>
                <div class="page-profil-info-address"><span>Адрес:</span>г. Ростов-на-Дону, ул. Советская 1</div>
                <div class="page-profil-info-email"><span>Email:</span> <a
                        href="add-page-profil-info-address">Добавить</a></div>
            </div>
        </div>

        <div class="clear"></div>
    </section>
    <section id="page-profil-account" class="center">
        <div class="page-profil-info-blockirovka"><img src="cabinet/icon/locked.png">Управление услугами</div>
        <table class="page-profil-table">
            <tr class="page-profil-table-title">
                <td>Услуга</td>
                <td>Тариф</td>
                <!-- <td><img src="/cabinet/icon/calendar.png">Дата</td> -->
                <td>Состояние</td>
                <td>Действие</td>
            </tr>
            <tr>
                <td>Интернет</td>
                <td>Социальный</td>
                <!-- <td><input type="date" value="2019-01-01"></td> -->
                <td>Активная</td>
                <td>
                    <div class="block-account">Заблокировать</div>
                </td>
            </tr>
            <tr>
                <td>Телевидение</td>
                <td>Оптимальный</td>
                <!-- <td><input type="date" value="2019-01-01"></td> -->
                <td>Заблокирована</td>
                <td>
                    <div class="unlock-account">Разблокировать</div>
                </td>
            </tr>
            <tr>
                <td>Статический (белый) адрес IpV4</td>
                <td>Премиальный</td>
                <!-- <td><input type="date" value="2019-01-01"></td> -->
                <td>Не подключена</td>
                <td>
                    <div class="connect-account">Подключить</div>
                </td>
            </tr>
        </table>
    </section>
    <section id="change-password" class="center">
        <h2>Смена пароля</h2>
        <form id="change-password-form" action="" method="post">
            <span class="change-password-label-block">
                <label for="change-password-current">
                    <span data-content="Текущий пароль">Текущий пароль</span>
                </label>
                <input type="password" id="change-password-current" placeholder="Текущий пароль"
                    name="change-password-current" required>

            </span>
            <span class="change-password-label-block">
                <label for="change-password-new">
                    <span data-content="Новый пароль">Новый пароль</span>
                </label>
                <input type="password" id="change-password-new" placeholder="Новый пароль" name="change-password-new"
                    required>

            </span>
            <span class="change-password-label-block">
                <label for="change-password-confirm">
                    <span data-content="Подтверждение пароля">Подтверждение пароля</span>
                </label>
                <input type="password" id="change-password-confirm" placeholder="Подтверждение пароля"
                    name="change-password-confirm" required>

            </span>
            <div class="change-password-button"><input type="submit" value="Сменить пароль"
                    onclick="yaCounterXXXXXXX.reachGoal(''); return true;"></div>
            <div class="clear"></div>
            <div class="correct-mini-text">Нажимая на кнопку «Сменить пароль», я даю своё согласие на <a
                    href="">обработку моих персональных данных</a> с целью испольнения заявки, уточнения параметров
                заявки. <a href="">Политика обработки и защиты персональных данных ООО «ИнТелКом»</a></div>
        </form>
    </section>
</main>
@endsection
