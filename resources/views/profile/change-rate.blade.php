@extends('layouts.app')
@section('content')
    <main id="change-tarif-main">
         <div class="change-tarif-main-title-bread center">
                 <h1>Сменить ТВ тариф</h1>
                <ol class="breadcrumb"><li class="breadcrumb-item"><a href="/">Главная</a></li><li class="breadcrumb-item"><a href="/profile">Профиль</a></li><li class="breadcrumb-item active">Сменить тариф интернета</li></ol>
         </div>
         <div class="main-tarif-card-section center">
            <div class="main-tarif-card-block">
                <h3>Мой тариф</h3>
                <div class="main-tarif-card-title">Социальный</div>
                <div class="main-tarif-card-pay">Ежемесячная плата: <span>90</span> руб./мес.</div>
                <div class="main-tarif-card-description"><p>94 канала.</p></div>
            </div>
            <div class="main-tarif-card-dop-list">
                <h3>Дополнительные пакеты</h3>
                <div class="main-tarif-card-dop-list-block">
                    <div class="main-tarif-card-dop-list-title col-md-5">
                        MEGOGO
                    </div>
                    <div class="main-tarif-card-dop-list-kanal col-md-2">
                    </div>
                    <div class="main-tarif-card-dop-list-price col-md-2">
                        <span>147</span> руб./мес.
                    </div>
                    <div class="main-tarif-card-dop-list-button col-md-3">
                        <div class="main-tarif-card-dop-list-connect">Подключить</div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="main-tarif-card-dop-list-block">
                    <div class="main-tarif-card-dop-list-title col-md-5">
                        МАТЧ ПРЕМЬЕР
                    </div>
                    <div class="main-tarif-card-dop-list-kanal col-md-2">
                        1 канал
                    </div>
                    <div class="main-tarif-card-dop-list-price col-md-2">
                         <span>219</span> руб./мес.
                    </div>
                    <div class="main-tarif-card-dop-list-button col-md-3">
                        <div class="main-tarif-card-dop-list-disconnect">Отключить</div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="main-tarif-card-dop-list-block">
                    <div class="main-tarif-card-dop-list-title col-md-5">
                        Дождь
                    </div>
                    <div class="main-tarif-card-dop-list-kanal col-md-2">
                        2 канала
                    </div>
                    <div class="main-tarif-card-dop-list-price col-md-2">
                        <span>240</span> руб./мес.
                    </div>
                    <div class="main-tarif-card-dop-list-button col-md-3">
                        <div class="main-tarif-card-dop-list-connect">Подключить</div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="main-tarif-card-dop-list-block">
                    <div class="main-tarif-card-dop-list-title col-md-5">
                        Амедиатека
                    </div>
                    <div class="main-tarif-card-dop-list-kanal col-md-2">
                        4 канала
                    </div>
                    <div class="main-tarif-card-dop-list-price col-md-2">
                        <span>299</span> руб./мес.
                    </div>
                    <div class="main-tarif-card-dop-list-button col-md-3">
                        <div class="main-tarif-card-dop-list-connect">Подключить</div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="main-tarif-card-dop-list-block">
                    <div class="main-tarif-card-dop-list-title col-md-5">
                        Для взрослых
                    </div>
                    <div class="main-tarif-card-dop-list-kanal col-md-2">
                        5 каналов
                    </div>
                    <div class="main-tarif-card-dop-list-price col-md-2">
                        <span>150</span> руб./мес.
                    </div>
                    <div class="main-tarif-card-dop-list-button col-md-3">
                        <div class="main-tarif-card-dop-list-connect">Подключить</div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="main-tarif-card-dop-list-block">
                    <div class="main-tarif-card-dop-list-title col-md-5">
                        МАТЧ! ФУТБОЛ
                    </div>
                    <div class="main-tarif-card-dop-list-kanal col-md-2">
                        6 каналов
                    </div>
                    <div class="main-tarif-card-dop-list-price col-md-2">
                        <span>380</span> руб./мес.
                    </div>
                    <div class="main-tarif-card-dop-list-button col-md-3">
                        <div class="main-tarif-card-dop-list-connect">Подключить</div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="main-tarif-card-dop-list-block">
                    <div class="main-tarif-card-dop-list-title col-md-5">
                        НАСТРОЙ КИНО
                    </div>
                    <div class="main-tarif-card-dop-list-kanal col-md-2">
                        8 каналов
                    </div>
                    <div class="main-tarif-card-dop-list-price col-md-2">
                        <span>319</span> руб./мес.
                    </div>
                    <div class="main-tarif-card-dop-list-button col-md-3">
                        <div class="main-tarif-card-dop-list-connect">Подключить</div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="main-tarif-card-dop-list-block">
                    <div class="main-tarif-card-dop-list-title col-md-5">
                        Увлекательный+
                    </div>
                    <div class="main-tarif-card-dop-list-kanal col-md-2">
                        17 каналов
                    </div>
                    <div class="main-tarif-card-dop-list-price col-md-2">
                        <span>100</span> руб./мес
                    </div>
                    <div class="main-tarif-card-dop-list-button col-md-3">
                        <div class="main-tarif-card-dop-list-connect">Подключить</div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
         </div>
        <section id="tarif-card-section">
            <div class="center">
                <h2>Доступные тарифы</h2>
                <div class="tarif-card-list-block">
                <div class="tarif-card-block">
                    <div class="tarif-card-back"><img class="svg" src="/cabinet/icon/friendship.svg"></div>
                    <div class="tarif-card-content">
                        <div class="tarif-card-title"><h2>Социальный</h2></div>
                        <div class="tarif-card-description col-md-6">Я могу каждый день искать различную информацию в интернете, читать новости, проверять почту, слушать музыку, общаться в социальных сетях. </div>
                        <div class="tarif-card-speed col-md-3">
                            <div class="tarif-card-speed-title">Телеканалов</div>
                            <div class="tarif-card-speed-info"><span>27</span> каналов</div>
                        </div>
                        <div class="tarif-card-price col-md-3">
                            <div class="tarif-card-speed-title">Абонентская плата</div>
                            <div class="tarif-card-speed-info"><span>1000</span> руб./мес.</div>
                        </div>
                        <div class="clear"></div>
                        <div class="tarif-card-button"><a href="">Подключить</a></div>
                    </div>
                </div>
                <div class="tarif-card-block">
                    <div class="tarif-card-back"><img class="svg" src="/cabinet/icon/family.svg"></div>
                    <div class="tarif-card-content">
                        <div class="tarif-card-title"><h2>Комфорт</h2></div>
                        <div class="tarif-card-description col-md-6">Всей семьей пользуемся интернетом на разных устройствах, часто скачиваем фильмы в высоком качестве, общаемся по видеосвязи (даже между собой)</div>
                        <div class="tarif-card-speed col-md-3">
                            <div class="tarif-card-speed-title">Телеканалов</div>
                            <div class="tarif-card-speed-info"><span>27</span> каналов</div>
                        </div>
                        <div class="tarif-card-price col-md-3">
                            <div class="tarif-card-speed-title">Абонентская плата</div>
                            <div class="tarif-card-speed-info"><span>1000</span> руб./мес.</div>
                        </div>
                        <div class="clear"></div>
                        <div class="tarif-card-button"><a href="">Подключить</a></div>
                    </div>
                </div>
                <div class="clear"></div>
                </div>
            </div>
        </section>
    </main>
@endsection