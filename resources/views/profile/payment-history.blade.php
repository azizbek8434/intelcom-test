@extends('layouts.app')
@section('content')
<main id="page-table">
    <section id="table-section" class="center">
        <h1>История платежей</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active">История платежей</li>
        </ol>
        <div class="download-history-sale">
            <div class="download-history-sale-block"><img src="/cabinet/icon/calendar.png">История платежей</div>
            <div id="search">
                <form method="post" action="?">
                    <div class="download-history-data-block">
                        <label for="search-from-date">c</label>
                        <input type="text" name="search-from-date" id="search-from-date" autocomplete="off" />
                    </div>

                    <div class="download-history-data-block">
                        <label for="search-to-date">по</label>
                        <input type="text" name="search-to-date" id="search-to-date" autocomplete="off" />
                    </div>

                    <div class="download-history-data-block">
                        <input type="submit" value="Найти" />
                    </div>
                </form>
            </div>
        </div>
        <table>
            <tr class="table-section-first-line">
                <td>
                    № п/п
                </td>
                <td>
                    № Лицевого счета для списания
                </td>
                <td>
                    Способ оплаты
                </td>
                <td>
                    Дата оплаты
                </td>
                <td>
                    Сумма оплаты
                </td>

            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="5">Нет результатов</td>
            </tr>
        </table>
    </section>
</main>
@endsection
