<!doctype html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>Регистрация авторизация</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="/cabinet/css/cabinet.css">
    <link rel="shortcut icon" href="/cabinet/favicon.gif" type="image/x-icon">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext"
        rel="stylesheet">
    <link rel="stylesheet" href="/cabinet/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="/cabinet/js/cabinet.js" type="text/javascript"></script>
</head>

<body>
    <header id="enter-registration-header">
        <div class="center">
            <div class="header-block"><a href="/"><img src="/cabinet/image/logo-white.png"></a></div>
        </div>
    </header>
    <main id="enter-registration">
        <section id="enter-ragistration-section">
            <div class="center">
                <div class="enter-ragistration-block">
                    <form id="enter-ragistration-form" method="post" autocomplete="on">
                        <div class="enter-ragistration-form-block">
                            <div class="enter-ragistration-form-label">
                                <label>Логин: </label>
                            </div>
                            <div class="enter-ragistration-form-input">
                                <input class="enter-ragistration-tel" name="enter-ragistration-tel" type="text"
                                    autocomplete="on">
                                <span class="enter-ragistration-tel-error">Неверный логин</span>
                            </div>
                            <div class="enter-ragistration-login-quest">
                                <img src="/cabinet/icon/information-tarif.png">
                                <div class="enter-ragistration-login-information">Укажите номер договора. Если Вы забыли
                                    номер, то уточните сообщением через мессенджеры на номер телефона +7-928-226-91-41
                                </div>
                            </div>
                        </div>
                        <div class="enter-ragistration-form-block">
                            <div class="enter-ragistration-form-label">
                                <label>Пароль:</label>
                            </div>
                            <div class="enter-ragistration-form-input">
                                <input class="enter-ragistration-password" name="enter-ragistration-password"
                                    type="password" autocomplete="on">
                                <span class="enter-ragistration-password-error">Неверный пароль</span>
                            </div>
                        </div>

												<div class="enter-ragistration-form-block">
                            <div class="enter-ragistration-form-label">
                                <label>Подтвердите Пароль:</label>
                            </div>
                            <div class="enter-ragistration-form-input">
                                <input class="enter-ragistration-password" name="enter-ragistration-password-confirm"
                                    type="password" autocomplete="on">
                                <span class="enter-ragistration-password-error">Неверный пароль</span>
                            </div>
                        </div>

                        <div class="enter-ragistration-checkbox">
                            <label class="label-check option">
                                <input type="checkbox" class="label-check__input">
                                <span class="label-check__new-input"></span>
                                Запомнить меня
                            </label>
                        </div>
                        <div class="enter-ragistration-form-submit"><input type="submit"
                                name="enter-ragistration-submit" value="Регистрация"></div>
                        <div class="enter-ragistration-form-conf">Нажимая на кнопку «Войти», Вы соглашаетесь с
                            обработкой персональных данных и политикой конфиденциальности</div>
                    </form>
                </div>
                <div class="about-us-cabinet">
                    <div class="about-us-cabinet-block">
                        <div class="about-us-cabinet-icon"><img src="/cabinet/icon/11connect.png"></div>
                        <div class="about-us-cabinet-text">Подключение услуг</div>
                    </div>
                    <div class="about-us-cabinet-block">
                        <div class="about-us-cabinet-icon"><img src="/cabinet/icon/11cash.png"></div>
                        <div class="about-us-cabinet-text">Оплата услуг</div>
                    </div>
                    <div class="about-us-cabinet-block">
                        <div class="about-us-cabinet-icon"><img src="/cabinet/icon/11mobile.png"></div>
                        <div class="about-us-cabinet-text">Смена тарифов</div>
                    </div>
                    <div class="about-us-cabinet-block">
                        <div class="about-us-cabinet-icon"><img src="/cabinet/icon/11statistic.png"></div>
                        <div class="about-us-cabinet-text">Контроль расходов</div>
                    </div>
                    <div class="about-us-cabinet-block">
                        <div class="about-us-cabinet-icon"><img src="/cabinet/icon/tag-mini.png"></div>
                        <div class="about-us-cabinet-text">Акции и бонусные программы</div>
                    </div>
                </div>
            </div>
        </section>
    </main>
		<script src="/cabinet/js/jquery.maskedinput.min.js"></script>
</body>

</html>
