<!doctype html>
<html lang="ru">
<head>
   <meta charset="UTF-8">
    <title>Регистрация авторизация</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="/cabinet/css/cabinet.css">
    <link rel="shortcut icon" href="/cabinet/favicon.gif" type="image/x-icon" >
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" href="/cabinet/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="/cabinet/js/cabinet.js" type="text/javascript"></script>
</head>
<body>
    <header id="enter-registration-header">
        <div class="center">
           <div class="header-block"><a href="/"><img src="/cabinet/image/logo-white.png"></a></div>
        </div>
    </header>
    <main id="enter-registration">
       <section id="enter-ragistration-section">
           <div class="center">
               <div class="enter-ragistration-block">
                   <form id="enter-ragistration-form" method="post" autocomplete="on">
                       <div class="enter-ragistration-form-block">
                           
                           <div class="enter-ragistration-form-label">
                               
                               <label>Логин: </label>
                               
                           </div>
                           <div class="enter-ragistration-form-input">
                               <input class="enter-ragistration-tel" name="enter-ragistration-tel" type="text" autocomplete="on" >
                               <span class="enter-ragistration-tel-error">Неверный логин</span>
                           </div>
                           <div class="enter-ragistration-login-quest">
                               <img src="/cabinet/icon/information-tarif.png" >
                               <div class="enter-ragistration-login-information">Укажите номер договора. Если Вы забыли номер, то уточните сообщением через мессенджеры на номер телефона +7-928-226-91-41</div>
                           </div>
                       </div>
                       <div class="enter-ragistration-form-block">
                           <div class="enter-ragistration-form-label">
                               <label>Пароль:</label>
                           </div>
                           <div class="enter-ragistration-form-input">
                               <input class="enter-ragistration-password" name="enter-ragistration-password" type="password" autocomplete="on">
                               <span class="enter-ragistration-password-error">Неверный пароль</span>
                           </div>
                       </div>
                       <div class="enter-ragistration-checkbox">
                           <label class="label-check option">
                              <input type="checkbox" class="label-check__input">
                              <span class="label-check__new-input"></span>
                              Запомнить меня
                            </label>
                        </div>
                       <div class="forget-password">Забыли пароль?</div>
                       <div class="enter-ragistration-form-submit"><input type="submit" name="enter-ragistration-submit" value="Войти"></div>
                       <div class="enter-ragistration-form-conf">Нажимая на кнопку «Войти», Вы соглашаетесь с обработкой персональных данных и политикой конфиденциальности</div>
                   </form >
               </div>
               <div class="about-us-cabinet">
                   <div class="about-us-cabinet-block">
                       <div class="about-us-cabinet-icon"><img src="/cabinet/icon/11connect.png"></div>
                       <a href="/profile/service-connection" class="about-us-cabinet-text">Подключение услуг</a>
                   </div>
                   <div class="about-us-cabinet-block">
                       <div class="about-us-cabinet-icon"><img src="/cabinet/icon/11cash.png"></div>
                       <div class="about-us-cabinet-text">Оплата услуг</div>
                   </div>
                   <div class="about-us-cabinet-block">
                       <div class="about-us-cabinet-icon"><img src="/cabinet/icon/11mobile.png"></div>
                       <a href="/profile/change-rate" class="about-us-cabinet-text">Смена тарифов</a></div>
                   <div class="about-us-cabinet-block">
                       <div class="about-us-cabinet-icon"><img src="/cabinet/icon/11statistic.png"></div>
                       <a href="/profile/payment-history" class="about-us-cabinet-text">Контроль расходов</a>
										</div>
                   <div class="about-us-cabinet-block">
                       <div class="about-us-cabinet-icon"><img src="/cabinet/icon/tag-mini.png"></div>
                  		 <a href="/posts/akcii-i-bonusnye-programmy" class="about-us-cabinet-text">Акции и бонусные программы</a></div>
									 </div>
               </div>
           </div>
       </section>
    </main>
<div class="forget-password-form">
    <div class="forget-password-back"></div>
    <form id="modal-form-forget-password" action="" method="post">
        <div class="close-form"><img src="/cabinet/icon/close-forn.png"></div>
        <div class="forget-password-text">Для восстановления пароля введите № договора. Информация о новом пароле будет передана Вам в СМС-сообщении, отправленном на телефон, указанный в вашем профиле или отправлена на почту.</div>
        <div class="forget-password-input-block form-num-dogovor">
            <label>№ договора</label>
            <input type="num" name="form-num-dogovor" required>
        </div>
        <div class="forget-password-input-block form-pas-radiob">
             <label>Восстановить через: </label>
            <div>
                <label class="forget-password-radio">
                    <input type="radio" name="form-forget-radiobox" value="Телефон" checked>
                    <span>Телефон</span>
                </label>
                <label class="forget-password-radio">
                    <input type="radio" name="form-forget-radiobox" value="Email">
                    <span>Email</span>
                </label>
            </div>
        </div>
        <div class="forget-password-submit"><input type="submit" value="Восстановить пароль"></div>
        <div class="enter-ragistration-form-conf">Нажимая на кнопку «Восстановить пароль», Вы соглашаетесь с обработкой персональных данных и политикой конфиденциальности</div>
    </form>
</div>
<script src="/cabinet/js/jquery.maskedinput.min.js"></script>
<script>
$('.close-form, .forget-password-back').on('click', function () {
        $('.forget-password-form').fadeOut('fast')
});
$(".forget-password").click(function() {
			$(".forget-password-form").fadeIn(300);
	     });
</script>
</body>
</html>
